use std::{
    cmp::PartialEq,
    fmt::{Debug, Display},
};

use crate::tree_node::{Iter, Link, RefIter, TreeNode};

/// A Map implemented as a Binary Search Tree
///
/// # Examples
///
/// ```
/// use hw_three::TreeMap;
///
/// // Perhaps we want to map integers to a str representation of themselves
/// let mut num_words = TreeMap::new();
///
/// num_words.insert(0, "Zero");
/// num_words.insert(1, "One");
/// num_words.insert(2, "Two");
/// num_words.insert(3, "Three");
///
/// // we can check for our number
/// if !num_words.contains_key(11) {
///     println!("It doesn't go to 11!");
/// }
///
/// ```
///
/// We can initialize from a Vec:
/// ```
/// use hw_three::TreeMap;
/// let ages = TreeMap::from(vec![
///     ("Henry", 33),
///     ("Allie", 35),
///     ("Lily", 2),
/// ]);
/// ```
pub struct TreeMap<K, V>
where
    K: Ord + PartialEq,
{
    root: Link<K, V>,
    size: usize,
}

impl<K, V> Default for TreeMap<K, V>
where
    K: Ord,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<K, V> TreeMap<K, V>
where
    K: Ord + PartialEq,
{
    /// Makes a new, empty `TreeMap`
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    ///
    /// // Entries can be added with insert
    /// map.insert("X", "Buried Treasure");
    /// ```
    pub fn new() -> Self {
        TreeMap {
            root: None,
            size: 0,
        }
    }

    /// Checks if the map is empty
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    ///
    /// assert!(map.is_empty());
    ///
    /// map.insert(1, 1);
    ///
    /// assert!(!map.is_empty());
    /// ```
    pub fn is_empty(&self) -> bool {
        self.root.is_none()
    }

    /// Returns the number of items in the map
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    ///
    /// assert_eq!(0, map.len());
    ///
    /// map.insert(1, 1);
    ///
    /// assert_eq!(1, map.len());
    /// ```
    pub fn len(&self) -> usize {
        self.size
    }

    /// Inserts a key-value pair into the map, overwriting the old value if key is present
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    ///
    /// map.insert(1, 1);
    /// assert_eq!(&1, map.get(1).unwrap());
    /// map.insert(1, 2);
    /// assert_eq!(&2, map.get(1).unwrap());
    /// ```
    pub fn insert(&mut self, key: K, value: V) {
        if let Some(root) = self.root.as_mut() {
            root.insert(key, value);
        } else {
            self.root = Some(TreeNode::new(key, value).boxed());
        }
        self.size += 1;
    }

    /// Inserts a key-value pair into the map
    /// If the key is already present, overwrites the value in a way
    /// prescribed by a function taking the old value and new value,
    /// and returning the actual new value to be inserted
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    ///
    /// map.insert_or(1, 1, |_, _| 7); // func never called
    /// assert_eq!(&1, map.get(1).unwrap());
    /// map.insert_or(1, 1, |old, new| old + new + 3);
    /// assert_eq!(&5, map.get(1).unwrap());
    /// ```
    pub fn insert_or<F>(&mut self, key: K, value: V, func: F)
    where
        F: Fn(&V, &V) -> V,
    {
        if let Some(root) = self.root.as_mut() {
            root.insert_or(key, value, func);
        } else {
            self.root = Some(TreeNode::new(key, value).boxed());
        }
        self.size += 1;
    }

    /// Checks if the map contains a key
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    ///
    /// assert!(!map.contains_key(1));
    ///
    /// map.insert(1, 1);
    ///
    /// assert!(map.contains_key(1));
    /// ```
    pub fn contains_key(&self, key: K) -> bool {
        self.get(key).is_some()
    }

    /// Returns a reference to the value for a given key, or None if the key is not present
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    /// assert!(map.get(1).is_none());
    /// map.insert(1, 1);
    /// assert_eq!(&1, map.get(1).unwrap());
    /// map.insert(1, 2);
    /// assert_eq!(&2, map.get(1).unwrap());
    /// ```
    pub fn get(&self, key: K) -> Option<&V> {
        if let Some(root) = self.root.as_ref() {
            root.get(key)
        } else {
            None
        }
    }

    /// Returns a mutable reference to the value for a given key, or None if the key is not present
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    ///
    /// let mut map = TreeMap::new();
    ///
    /// map.insert(1, 1);
    /// assert_eq!(&1, map.get(1).unwrap());
    /// *map.get_mut(1).unwrap() = 2;
    /// assert_eq!(&2, map.get(1).unwrap());
    /// ```
    pub fn get_mut(&mut self, key: K) -> Option<&mut V> {
        if let Some(root) = self.root.as_mut() {
            root.get_mut(key)
        } else {
            None
        }
    }

    /// Clears out all entries in the tree
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    /// let mut points = TreeMap::from(vec![
    ///     ("Dave", 66),
    ///     ("Charlie", 47),
    ///     ("Lyle", 42),
    /// ]);
    /// points.clear();
    /// assert!(points.is_empty());
    /// ```
    pub fn clear(&mut self) {
        self.root = None;
        self.size = 0;
    }

    /// Returns a tuple of the key-value pair of the smallest/first/least key in the map
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    /// let mut points = TreeMap::from(vec![
    ///     ("Dave", 66),
    ///     ("Charlie", 47),
    ///     ("Lyle", 42),
    /// ]);
    ///
    /// // Alphabetical!
    /// assert_eq!((&"Charlie", &47), points.first_key_value().unwrap());
    /// ```
    pub fn first_key_value(&self) -> Option<(&K, &V)> {
        self.root.as_ref().map(|root| root.min_key_value())
    }

    /// Returns a tuple of the key-value pair of the biggest/last/greatest key in the map
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    /// let mut medals = TreeMap::from(vec![
    ///     (3, "Bronze"),
    ///     (2, "Silver"),
    ///     (1, "Gold"),
    /// ]);
    ///
    /// assert_eq!((&3, &"Bronze"), medals.last_key_value().unwrap());
    /// ```
    pub fn last_key_value(&self) -> Option<(&K, &V)> {
        // This was suggested by clippy, kind of a cool way to one line this.
        self.root.as_ref().map(|root| root.max_key_value())
    }

    /// Removes the entry belonging to a given key from the map
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    /// let mut squares = TreeMap::from(vec![
    ///     (1, 1),
    ///     (2, 4),
    ///     (3, 9),
    ///     (4, 16),
    /// ]);
    ///
    /// squares.remove(&3);
    /// assert!(!squares.contains_key(3));
    /// ```
    pub fn remove(&mut self, key: &K) -> Option<(K, V)> {
        if let Some(root) = self.root.take() {
            let (leave, take) = TreeNode::remove(root, key);
            self.root = leave;
            self.size -= 1;
            Some(take?.yield_values())
        } else {
            None
        }
    }

    /// Removes the entry belonging to the last key from the map
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    /// let mut squares = TreeMap::from(vec![
    ///     (1, 1),
    ///     (2, 4),
    ///     (3, 9),
    ///     (4, 16),
    /// ]);
    ///
    /// squares.remove_first();
    /// assert!(!squares.contains_key(1));
    /// ```
    pub fn remove_first(&mut self) -> Option<(K, V)> {
        if let Some(root) = self.root.take() {
            let (leave, take) = TreeNode::remove_first(root);
            self.root = leave;
            self.size -= 1;
            Some(take?.yield_values())
        } else {
            None
        }
    }

    /// Removes the entry belonging to the last key from the map
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_three::TreeMap;
    /// let mut squares = TreeMap::from(vec![
    ///     (1, 1),
    ///     (2, 4),
    ///     (3, 9),
    ///     (4, 16),
    /// ]);
    ///
    /// squares.remove_last();
    /// assert!(!squares.contains_key(4));
    /// ```
    pub fn remove_last(&mut self) -> Option<(K, V)> {
        if let Some(root) = self.root.take() {
            let (leave, take) = TreeNode::remove_last(root);
            self.root = leave;
            self.size -= 1;
            Some(take?.yield_values())
        } else {
            None
        }
    }

    /// Return an iterator borrowing nodes from the tree
    pub fn iter(&self) -> RefIter<K, V> {
        if let Some(root) = &self.root {
            RefIter::new(root)
        } else {
            RefIter::empty()
        }
    }

    /// Return an iterator consuming nodes from the tree
    fn into_iter(self) -> Iter<K, V> {
        if let Some(root) = self.root {
            Iter::new(root)
        } else {
            Iter::empty()
        }
    }

    // Return an iterator mutably borrowing nodes from the tree
    // fn iter_mut(&mut self) {
    //     todo!();
    // }
}

impl<'a, K, V> IntoIterator for &'a TreeMap<K, V>
where
    K: Ord
{
    type Item = (&'a K, &'a V);
    type IntoIter = RefIter<'a, K, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<K, V> IntoIterator for TreeMap<K, V>
where
    K: Ord
{
    type Item = (K, V);
    type IntoIter = Iter<K, V>;

    fn into_iter(self) -> Self::IntoIter {
        self.into_iter()
    }
}

impl<K, V> std::ops::Index<K> for TreeMap<K, V>
where
    K: Ord,
{
    type Output = V;

    /// Returns a reference to the value for a given key
    ///
    /// # Panics
    ///
    /// Panics if the key isn't in the map
    fn index(&self, index: K) -> &V {
        self.get(index).expect("No entry found for key")
    }
}

impl<K, V> std::ops::IndexMut<K> for TreeMap<K, V>
where
    K: Ord,
{
    /// Returns a mutable reference to the value for a given key
    ///
    /// # Panics
    ///
    /// Panics if the key isn't in the map
    fn index_mut(&mut self, index: K) -> &mut V {
        self.get_mut(index).expect("No entry found for key")
    }
}

impl<K, V> core::fmt::Debug for TreeMap<K, V>
where
    K: Ord + Debug,
    V: Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&format!(
            "TreeMap {{ size: {:?}, root: {:?} }}",
            self.size, self.root
        ))
    }
}

impl<K, V> core::fmt::Display for TreeMap<K, V>
where
    K: Ord + Display,
    V: Display,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let root = match self.root.as_ref() {
            Some(root) => format!("{}", root),
            None => "None".to_string(),
        };
        f.write_str(&format!("TreeMap: size: {}, nodes: {}", self.size, root))
    }
}

impl<K, V> From<Vec<(K, V)>> for TreeMap<K, V>
where
    K: Ord,
{
    /// Constructs a new TreeMap from a Vec of tuples
    ///
    /// # Examples
    ///
    /// Both of the following mechanisms result in the same map
    /// ```
    /// use hw_three::TreeMap;
    /// let num_names = TreeMap::from(vec![
    ///     (1, "one"),
    ///     (2, "two"),
    ///     (3, "three"),
    /// ]);
    ///
    /// let mut num_words = TreeMap::new();
    ///
    /// num_words.insert(1, "one");
    /// num_words.insert(2, "two");
    /// num_words.insert(3, "three");
    ///
    /// ```
    fn from(value: Vec<(K, V)>) -> Self {
        let mut new_tree = TreeMap::new();
        for (key, value) in value {
            new_tree.insert(key, value);
        }
        new_tree
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_and_empty() {
        let test_tree: TreeMap<usize, usize> = TreeMap::new();
        assert!(test_tree.is_empty());
        assert_eq!(0, test_tree.len());
    }

    #[test]
    fn test_insert_and_len() {
        let mut test_tree: TreeMap<usize, usize> = TreeMap::new();
        assert_eq!(0, test_tree.len());
        test_tree.insert(1, 1);
        assert_eq!(1, test_tree.len());
        test_tree.insert(2, 2);
        assert_eq!(2, test_tree.len());
        assert!(!test_tree.is_empty());
        test_tree.insert(2, 0);
    }

    #[test]
    fn test_get() {
        let mut test_tree: TreeMap<usize, usize> = TreeMap::new();
        assert!(test_tree.is_empty());
        assert!(test_tree.get(1).is_none());
        test_tree.insert(1, 1);
        assert_eq!(&1, test_tree.get(1).unwrap());
        test_tree.insert(2, 2);
        assert_eq!(&2, test_tree.get(2).unwrap());
        assert!(test_tree.get(3).is_none());
    }

    #[test]
    fn test_get_mut() {
        let mut test_tree: TreeMap<usize, String> = TreeMap::new();
        assert!(test_tree.is_empty());
        test_tree.insert(1, "One".into());
        assert_eq!(&String::from("One"), test_tree.get(1).unwrap());
        let val_ref = test_tree.get_mut(1).unwrap();
        val_ref.push_str(" and done!");
        assert_eq!(&String::from("One and done!"), test_tree.get(1).unwrap());
        let mut test_tree: TreeMap<usize, usize> = TreeMap::new();
        assert!(test_tree.is_empty());
        test_tree.insert(1, 1);
        assert_eq!(&1, test_tree.get(1).unwrap());
        let val_ref = test_tree.get_mut(1).unwrap();
        *val_ref = 2;
        assert_eq!(&2, test_tree.get(1).unwrap());
        assert!(test_tree.get(3).is_none());
    }

    #[test]
    fn test_insert_or() {
        let pairs = vec![(1, 1), (2, 1), (3, 1)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);

        // Test behavior if key is present
        test_tree.insert_or(1, 1, |a, b| a + b);
        assert_eq!(&2, test_tree.get(1).unwrap());
        test_tree.insert_or(1, 2, |a, b| a * b);
        assert_eq!(&4, test_tree.get(1).unwrap());

        // Test discarding the new intended value and only act on old
        test_tree.insert_or(2, 7, |a, _| a * 3);
        assert_eq!(&3, test_tree.get(2).unwrap());
        // Test discarding both values and do something totally new
        test_tree.insert_or(3, 6, |_, _| 42);
        assert_eq!(&42, test_tree.get(3).unwrap());

        // Test behavior if key is absent
        test_tree.insert_or(4, 1, |a, b| a + b);
        assert_eq!(&1, test_tree.get(4).unwrap());
    }

    #[test]
    fn test_contains_key() {
        let pairs = vec![(1, 1), (2, 1), (3, 1)];
        let test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        assert!(test_tree.contains_key(1));
        assert!(!test_tree.contains_key(0));
    }

    #[test]
    fn test_clear() {
        let pairs = vec![(1, 1), (2, 1), (3, 1)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        assert_eq!(3, test_tree.len());
        test_tree.clear();
        assert!(test_tree.is_empty());
        assert_eq!(0, test_tree.len());
    }

    #[test]
    fn test_first_and_last_key_vals() {
        let pairs = vec![(1, 1), (7, 7), (3, 3), (16, 16), (2, 2)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);

        // Test correct finding of first and last
        assert_eq!((&1, &1), test_tree.first_key_value().unwrap());
        assert_eq!((&16, &16), test_tree.last_key_value().unwrap());

        // Insert and test again
        test_tree.insert(0, 0);
        test_tree.insert(19, 19);
        assert_eq!((&0, &0), test_tree.first_key_value().unwrap());
        assert_eq!((&19, &19), test_tree.last_key_value().unwrap());

        // Clear and test again
        test_tree.clear();
        assert!(test_tree.first_key_value().is_none());
        assert!(test_tree.last_key_value().is_none());
    }

    #[test]
    fn test_index() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        assert_eq!(1, test_tree[1]);
        assert_eq!(2, test_tree[2]);
        assert_eq!(3, test_tree[3]);
    }

    #[test]
    fn test_index_mut() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        test_tree[1] = 7;
        assert_eq!(7, test_tree[1]);
        assert_eq!(2, test_tree[2]);
        assert_eq!(3, test_tree[3]);
    }

    #[test]
    #[should_panic]
    fn test_index_panics() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        test_tree[0];
    }

    #[test]
    #[should_panic]
    fn test_index_mut_panics() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        test_tree[0] = 7;
    }

    #[test]
    fn test_debug() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        assert_eq!(
            "TreeMap { size: 3, root: \
            Some(TreeNode { key: 1, value: 1, \
                left_child: None, \
                right_child: Some(TreeNode { key: 2, value: 2, \
                    left_child: None, \
                    right_child: Some(TreeNode { key: 3, value: 3, \
                        left_child: None, \
                        right_child: None }) }) }) }",
            format!("{:?}", test_tree)
        );
    }

    #[test]
    fn test_display() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        assert_eq!(
            "TreeMap: size: 3, nodes: <1: 1> <2: 2> <3: 3>",
            format!("{}", test_tree)
        );
    }

    #[test]
    fn test_remove() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        // Remove an existing key
        let (key, value) = test_tree.remove(&2).unwrap();
        assert_eq!(2, key);
        assert_eq!(2, value);
        assert!(!test_tree.contains_key(2));
        assert_eq!(2, test_tree.len());
        // Attempt to remove a non-existent key
        assert!(test_tree.remove(&7).is_none())
    }

    #[test]
    fn test_remove_first() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        let (key, value) = test_tree.remove_first().unwrap();
        assert_eq!(1, key);
        assert_eq!(1, value);
        assert!(!test_tree.contains_key(1));
        assert_eq!(2, test_tree.len());

        let mut empty_tree: TreeMap<usize, usize> = TreeMap::new();
        assert!(empty_tree.remove_first().is_none())
    }

    #[test]
    fn test_remove_last() {
        let pairs = vec![(1, 1), (2, 2), (3, 3)];
        let mut test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        let (key, value) = test_tree.remove_last().unwrap();
        assert_eq!(3, key);
        assert_eq!(3, value);
        assert!(!test_tree.contains_key(3));
        assert_eq!(2, test_tree.len());

        let mut empty_tree: TreeMap<usize, usize> = TreeMap::new();
        assert!(empty_tree.remove_last().is_none())
    }

    #[test]
    fn test_remove_internal_sanity() {
        let pairs = vec![
            (5, 5),
            (2, 2),
            (8, 8),
            (0, 0),
            (1, 1),
            (3, 3),
            (7, 7),
            (4, 4),
            (6, 6),
            (10, 10),
            (9, 9),
        ];
        let mut test_tree1: TreeMap<usize, usize> = TreeMap::from(pairs.clone());
        let mut test_tree2: TreeMap<usize, usize> = TreeMap::from(pairs.clone());
        assert_eq!(
            "TreeMap: size: 11, nodes: <0: 0> <1: 1> <2: 2> <3: 3> <4: 4> <5: 5> <6: 6> <7: 7> <8: 8> <9: 9> <10: 10>",
            format!("{}", test_tree1)
        );
        for i in 0..=10 {
            let (key_first, _) = test_tree1.remove_first().unwrap();
            assert_eq!(i, key_first);
            let (key_last, _) = test_tree2.remove_last().unwrap();
            assert_eq!(10 - i, key_last);
        }
        let mut test_tree3: TreeMap<usize, usize> = TreeMap::from(pairs);
        test_tree3.remove(&2);
        assert_eq!(
            "TreeMap: size: 10, nodes: <0: 0> <1: 1> <3: 3> <4: 4> <5: 5> <6: 6> <7: 7> <8: 8> <9: 9> <10: 10>",
            format!("{}", test_tree3)
        );
        test_tree3.remove(&5);
        assert_eq!(
            "TreeMap: size: 9, nodes: <0: 0> <1: 1> <3: 3> <4: 4> <6: 6> <7: 7> <8: 8> <9: 9> <10: 10>",
            format!("{}", test_tree3)
        );
        assert_eq!(&6, test_tree3.root.unwrap().key_value().0)
    }

    #[test]
    fn test_iter() {
        let pairs = vec![(5, 5), (2, 2), (8, 8)];
        let test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        let mut test_iter = test_tree.iter();
        assert_eq!(Some((&2, &2)), test_iter.next());
        assert_eq!(Some((&5, &5)), test_iter.next());
        assert_eq!(Some((&8, &8)), test_iter.next());
        assert_eq!(None, test_iter.next());
        assert_eq!(3, test_tree.len())
    }

    #[test]
    fn test_into_iter() {
        let pairs = vec![(5, 5), (2, 2), (8, 8)];
        let test_tree: TreeMap<usize, usize> = TreeMap::from(pairs);
        println!("{}", test_tree);
        let mut test_iter = test_tree.into_iter();
        assert_eq!(Some((2, 2)), test_iter.next());
        assert_eq!(Some((5, 5)), test_iter.next());
        assert_eq!(Some((8, 8)), test_iter.next());
        assert_eq!(None, test_iter.next());
    }
}

/// Supporting module describing the nodes in the BST
/// Code contained within this by convention does not generate documention
/// because how this is implemented shouldn't matter to our public API
mod tree_node {
    use std::fmt::{Debug, Display};

    // This line is stolen shamelessly from the bst implementation in the rust nomicon:
    // https://doc.rust-lang.org/nomicon/borrow-splitting.html
    pub type Link<K, V> = Option<Box<TreeNode<K, V>>>;

    pub struct TreeNode<K, V>
    where
        K: PartialOrd + PartialEq,
    {
        key: K,
        value: V,
        left_child: Link<K, V>,
        right_child: Link<K, V>,
    }

    impl<K, V> TreeNode<K, V>
    where
        K: Ord,
    {
        pub fn new(key: K, value: V) -> Self {
            TreeNode {
                key,
                value,
                left_child: None,
                right_child: None,
            }
        }

        /// Convenience method for boxing
        pub fn boxed(self) -> Box<Self> {
            Box::new(self)
        }

        /// Convenience method for referencing this nodes k,v pair
        pub fn key_value(&self) -> (&K, &V) {
            (&self.key, &self.value)
        }

        pub fn insert(&mut self, key: K, value: V) {
            match self.key.cmp(&key) {
                std::cmp::Ordering::Greater => {
                    if let Some(l_child) = self.left_child.as_mut() {
                        l_child.insert(key, value)
                    } else {
                        self.left_child = Some(Box::new(TreeNode::new(key, value)));
                    }
                }
                std::cmp::Ordering::Equal => self.value = value,
                std::cmp::Ordering::Less => {
                    if let Some(r_child) = self.right_child.as_mut() {
                        r_child.insert(key, value)
                    } else {
                        self.right_child = Some(Box::new(TreeNode::new(key, value)));
                    }
                }
            }
        }

        pub fn insert_or<F>(&mut self, key: K, value: V, func: F)
        where
            F: Fn(&V, &V) -> V,
        {
            match self.key.cmp(&key) {
                std::cmp::Ordering::Greater => {
                    if let Some(l_child) = self.left_child.as_mut() {
                        l_child.insert_or(key, value, func)
                    } else {
                        self.left_child = Some(Box::new(TreeNode::new(key, value)));
                    }
                }
                std::cmp::Ordering::Equal => self.value = func(&self.value, &value),
                std::cmp::Ordering::Less => {
                    if let Some(r_child) = self.right_child.as_mut() {
                        r_child.insert_or(key, value, func)
                    } else {
                        self.right_child = Some(Box::new(TreeNode::new(key, value)));
                    }
                }
            }
        }

        pub fn get(&self, key: K) -> Option<&V> {
            match self.key.cmp(&key) {
                std::cmp::Ordering::Greater => {
                    if let Some(l_child) = self.left_child.as_ref() {
                        l_child.get(key)
                    } else {
                        None
                    }
                }
                std::cmp::Ordering::Equal => Some(&self.value),
                std::cmp::Ordering::Less => {
                    if let Some(r_child) = self.right_child.as_ref() {
                        r_child.get(key)
                    } else {
                        None
                    }
                }
            }
        }

        pub fn get_mut(&mut self, key: K) -> Option<&mut V> {
            match self.key.cmp(&key) {
                std::cmp::Ordering::Greater => {
                    if let Some(l_child) = self.left_child.as_mut() {
                        l_child.get_mut(key)
                    } else {
                        None
                    }
                }
                std::cmp::Ordering::Equal => Some(&mut self.value),
                std::cmp::Ordering::Less => {
                    if let Some(r_child) = self.right_child.as_mut() {
                        r_child.get_mut(key)
                    } else {
                        None
                    }
                }
            }
        }

        pub fn min_key_value(&self) -> (&K, &V) {
            if let Some(l_child) = self.left_child.as_ref() {
                l_child.min_key_value()
            } else {
                self.key_value()
            }
        }

        pub fn max_key_value(&self) -> (&K, &V) {
            if let Some(r_child) = self.right_child.as_ref() {
                r_child.max_key_value()
            } else {
                self.key_value()
            }
        }

        pub fn remove(mut node: Box<Self>, key: &K) -> (Link<K, V>, Link<K, V>) {
            match node.key.cmp(key) {
                std::cmp::Ordering::Less => {
                    if let Some(r_child) = node.right_child {
                        let (leave, take) = TreeNode::remove(r_child, key);
                        node.right_child = leave;
                        (Some(node), take)
                    } else {
                        (Some(node), None)
                    }
                }
                std::cmp::Ordering::Equal => TreeNode::pluck_node(node),
                std::cmp::Ordering::Greater => {
                    if let Some(l_child) = node.left_child {
                        let (leave, take) = TreeNode::remove(l_child, key);
                        node.left_child = leave;
                        (Some(node), take)
                    } else {
                        (Some(node), None)
                    }
                }
            }
        }
        pub fn remove_first(mut node: Box<Self>) -> (Link<K, V>, Link<K, V>) {
            if let Some(l_child) = node.left_child {
                let (leave, take) = TreeNode::remove_first(l_child);
                node.left_child = leave;
                (Some(node), take)
            } else {
                TreeNode::pluck_node(node)
            }
        }

        pub fn remove_last(mut node: Box<Self>) -> (Link<K, V>, Link<K, V>) {
            if let Some(r_child) = node.right_child {
                let (leave, take) = TreeNode::remove_last(r_child);
                node.right_child = leave;
                (Some(node), take)
            } else {
                TreeNode::pluck_node(node)
            }
        }

        fn pluck_node(mut node: Box<Self>) -> (Link<K, V>, Link<K, V>) {
            match (node.left_child.take(), node.right_child.take()) {
                (None, None) => (None, Some(node)),
                (None, Some(r_child)) => (Some(r_child), Some(node)),
                (Some(l_child), None) => (Some(l_child), Some(node)),
                (Some(l_child), Some(r_child)) => {
                    let (leave, mut take) = TreeNode::remove_first(r_child);
                    // Take is guaranteed to not be None, by virtue of the pattern matching enforcing that r_child is Some,
                    // Combined with the fact that remove last's base case is to return the node itself, in this case, r_child
                    take.as_mut().unwrap().left_child = Some(l_child);
                    take.as_mut().unwrap().right_child = leave;
                    (take, Some(node))
                }
            }
        }

        pub fn yield_values(self) -> (K, V) {
            (self.key, self.value)
        }

        pub fn destructure(self) -> (K, V, Link<K, V>, Link<K, V>) {
            (self.key, self.value, self.left_child, self.right_child)
        }
    }

    pub struct RefIter<'a, K, V>
    where
        K: Ord,
    {
        breadcrumbs: Vec<&'a TreeNode<K, V>>,
        next: Option<&'a TreeNode<K, V>>,
    }

    impl<'a, K, V> RefIter<'a, K, V>
    where
        K: Ord,
    {
        pub fn new(root: &'a TreeNode<K, V>) -> Self {
            let mut breadcrumbs: Vec<&TreeNode<K, V>> = vec![root];
            let mut next = root;
            while let Some(lefter) = &next.left_child {
                breadcrumbs.push(lefter);
                next = lefter;
            }
            let next = breadcrumbs.pop();
            RefIter { breadcrumbs, next }
        }

        pub fn empty() -> Self {
            RefIter {
                breadcrumbs: Vec::new(),
                next: None,
            }
        }
    }

    impl<'a, K, V> Iterator for RefIter<'a, K, V>
    where
        K: Ord,
    {
        type Item = (&'a K, &'a V);

        fn next(&mut self) -> Option<Self::Item> {
            if let Some(next) = self.next {
                if let Some(r_child) = &next.right_child {
                    self.breadcrumbs.push(r_child);
                    let mut next = r_child;
                    while let Some(lefter) = &next.left_child {
                        self.breadcrumbs.push(lefter);
                        next = lefter;
                    }
                }
                self.next = self.breadcrumbs.pop();
                Some(next.key_value())
            } else {
                None
            }
        }
    }

    pub struct Iter<K, V>
    where
        K: Ord
    {
        breadcrumbs: Vec<Box<TreeNode<K, V>>>,
        next: Link<K, V>,
    }

    impl<K, V> Iter<K, V>
    where
        K: Ord
    {
        pub fn new(root: Box<TreeNode<K, V>>) -> Self {
            let mut breadcrumbs = Self::take_next(root);
            let next = breadcrumbs.pop();
            Iter { breadcrumbs, next }
        }

        fn take_next(mut root: Box<TreeNode<K, V>>) -> Vec<Box<TreeNode<K, V>>> {
            let mut breadcrumbs: Vec<Box<TreeNode<K, V>>> = Vec::new();
            if let Some(next) = root.left_child.take() {
                breadcrumbs.push(root);
                breadcrumbs.append(&mut Self::take_next(next));
            } else {
                breadcrumbs.push(root);
            }
            breadcrumbs
        }

        pub fn empty() -> Self {
            Iter {
                breadcrumbs: Vec::new(),
                next: None,
            }
        }
    }

    impl<K, V> Iterator for Iter<K, V>
    where
        K: Ord
    {
        type Item = (K, V);

        fn next(&mut self) -> Option<Self::Item> {
            if let Some(next) = self.next.take() {
                let next_members = next.destructure();
                if next_members.2.is_some() {
                    panic!("Invarient violated, leftmost node has left child");
                }
                if let Some(r_child) = next_members.3 {
                    self.breadcrumbs.append(&mut Self::take_next(r_child));
                }
                self.next = self.breadcrumbs.pop();
                Some((next_members.0, next_members.1))
            } else {
                None
            }
        }
    }

    // pub struct MutIter<'a, K, V>
    // where
    //     K: Ord,
    // {
    //     breadcrumbs: Vec<&'a Link<K, V>>,
    //     next: Link<K, V>,
    //     current: Link<K, V>,
    // }

    // impl<'a, K, V> MutIter<'a, K, V>
    // where
    //     K: Ord,
    // {
    //     pub fn new(root: &'a Link<K, V>) -> Self {
    //         let mut breadcrumbs: Vec<&Link<K, V>> = vec![root];
    //         let mut next = root;
    //         while let Some(inner) = next {
    //             if let Some(l_child) = &inner.left_child {

    //             }
    //         }
    //         let next = breadcrumbs.pop().and_then(|link| *link);
    //         MutIter { breadcrumbs, next, current: None }
    //     }

    //     pub fn empty() -> Self {
    //         MutIter {
    //             breadcrumbs: Vec::new(),
    //             next: None,
    //             current: None,
    //         }
    //     }
    // }

    // impl<'a, K: 'a, V: 'a> Iterator for MutIter<'a, K, V>
    // where
    //     K: Ord,
    // {
    //     type Item = (&'a K, &'a mut V);

    //     fn next(&mut self) -> Option<Self::Item> {
    //         if let Some(mut next) = &self.next {
    //             self.current = Some(next);
    //             if let Some(r_child) = &next.right_child {
    //                 self.breadcrumbs.push(&next.right_child);
    //                 let next = r_child;
    //                 while let Some(lefter) = &next.left_child {
    //                     self.breadcrumbs.push(&next.left_child);
    //                     next = lefter;
    //                 }
    //             }
    //             self.next = self.breadcrumbs.pop().and_then(|link| *link);
    //             self.current.as_mut().map(|node| (&node.key, &mut node.value))
    //         } else {
    //             None
    //         }
    //     }
    // }

    impl<K, V> PartialEq for TreeNode<K, V>
    where
        K: PartialOrd + PartialEq,
    {
        fn eq(&self, other: &Self) -> bool {
            self.key == other.key
        }
    }

    impl<K, V> Eq for TreeNode<K, V> where K: Ord + PartialEq {}

    impl<K, V> PartialOrd for TreeNode<K, V>
    where
        K: PartialOrd + PartialEq,
    {
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            self.key.partial_cmp(&other.key)
        }
    }

    impl<K, V> Ord for TreeNode<K, V>
    where
        K: Ord + PartialEq,
    {
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            self.key.cmp(&other.key)
        }
    }

    impl<K, V> core::fmt::Debug for TreeNode<K, V>
    where
        K: Ord + Debug,
        V: Debug,
    {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            f.write_str(&format!(
                "TreeNode {{ key: {:?}, value: {:?}, left_child: {:?}, right_child: {:?} }}",
                self.key, self.value, self.left_child, self.right_child
            ))
        }
    }

    impl<K, V> core::fmt::Display for TreeNode<K, V>
    where
        K: Ord + Display,
        V: Display,
    {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            if let Some(l_child) = self.left_child.as_ref() {
                f.write_str(&format!("{} ", l_child))?;
            }
            f.write_str(&format!("<{}: {}>", self.key, self.value))?;
            if let Some(r_child) = self.right_child.as_ref() {
                f.write_str(&format!(" {}", r_child))?;
            }
            Ok(())
        }
    }

    #[cfg(test)]
    mod tests {
        use super::*;

        #[test]
        fn test_insert() {
            let mut test_node = TreeNode::new(1, 1);

            // Test insertion of child nodes
            // Here I had a learning moment about Option.as_ref and the need to use
            // it to peek at the value inside an option without a partial move occurring
            test_node.insert(2, 2);
            assert_eq!(2, test_node.right_child.as_ref().unwrap().key);
            test_node.insert(0, 0);
            assert_eq!(0, test_node.left_child.as_ref().unwrap().key);

            // Test value overwrite
            test_node.insert(2, 4);
            assert_eq!(2, test_node.right_child.as_ref().unwrap().key);
        }

        #[test]
        fn test_insert_or() {
            let mut test_node = TreeNode::new(1, 1);

            // Test behavior if key is present
            test_node.insert_or(1, 1, |a, b| a + b);
            assert_eq!(&2, test_node.get(1).unwrap());

            // Test behavior if key is absent
            test_node.insert_or(4, 1, |a, b| a + b);
            println!("{}", test_node);
            assert_eq!(&1, test_node.get(4).unwrap());
        }

        #[test]
        fn test_get() {
            let mut test_node = TreeNode::new(1, 1);
            assert!(test_node.get(2).is_none());
            assert_eq!(&1, test_node.get(1).unwrap());
            test_node.insert(2, 2);
            assert_eq!(&2, test_node.get(2).unwrap());
        }

        #[test]
        fn test_get_mut() {
            let mut test_node = TreeNode::new(1, 1);
            let val_ref = test_node.get_mut(1).unwrap();
            *val_ref = 2;
            assert_eq!(&2, test_node.get(1).unwrap());
            assert!(test_node.get(3).is_none());
        }

        #[test]
        fn test_min_max() {
            let mut test_node = TreeNode::new(1, 1);

            // When it has no children, it is both min and max
            assert_eq!(&1, test_node.min_key_value().0);
            assert_eq!(&1, test_node.max_key_value().0);

            // But the children should be correctly filed by value
            test_node.insert(2, 2);
            assert_eq!(&2, test_node.max_key_value().0);
            assert_eq!(&1, test_node.min_key_value().0);
            test_node.insert(0, 0);
            assert_eq!(&0, test_node.min_key_value().0);
        }

        #[test]
        fn test_debug() {
            let mut test_node = TreeNode::new(1, 1);
            assert_eq!(
                "TreeNode { key: 1, value: 1, left_child: None, right_child: None }",
                format!("{:?}", test_node)
            );
            test_node.insert(2, 2);
            assert_eq!(
                "TreeNode { key: 1, value: 1, left_child: None, right_child: Some(TreeNode { key: 2, value: 2, left_child: None, right_child: None }) }",
                format!("{:?}", test_node)
            );
        }

        #[test]
        fn test_display() {
            let mut test_node = TreeNode::new(1, 1);
            assert_eq!("<1: 1>", format!("{}", test_node));
            test_node.insert(2, 2);
            assert_eq!("<1: 1> <2: 2>", format!("{}", test_node));
            test_node.insert(0, 0);
            assert_eq!("<0: 0> <1: 1> <2: 2>", format!("{}", test_node));
        }

        #[test]
        fn test_remove() {
            let mut test_node = TreeNode::new(1, 1);
            test_node.insert(2, 2);
            let (test_node, taken) = TreeNode::remove(test_node.boxed(), &1);
            let (key, value) = taken.unwrap().yield_values();
            assert_eq!(1, key);
            assert_eq!(1, value);
            assert_eq!(2, test_node.unwrap().key)
        }

        #[test]
        fn test_remove_first() {
            let mut test_node = TreeNode::new(1, 1);
            test_node.insert(2, 2);
            let (test_node, taken) = TreeNode::remove_first(test_node.boxed());
            let (key, value) = taken.unwrap().yield_values();
            assert_eq!(1, key);
            assert_eq!(1, value);
            assert_eq!(2, test_node.unwrap().key)
        }

        #[test]
        fn test_remove_last() {
            let mut test_node = TreeNode::new(1, 1);
            test_node.insert(2, 2);
            let (test_node, taken) = TreeNode::remove_last(test_node.boxed());
            let (key, value) = taken.unwrap().yield_values();
            assert_eq!(2, key);
            assert_eq!(2, value);
            assert_eq!(1, test_node.unwrap().key)
        }

        #[test]
        fn test_pluck_node() {
            let mut test_node = TreeNode::new(1, 1).boxed();
            test_node.insert(2, 2);
            let (test_node, old_node) = TreeNode::pluck_node(test_node);
            let (key, value) = old_node.unwrap().yield_values();
            assert_eq!(1, key);
            assert_eq!(1, value);
            assert_eq!(2, test_node.unwrap().key)
        }
    }
}

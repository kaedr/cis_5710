use std::io;

/// DISCLAIMER:
/// Let the record show this would be much cleaner and idiomatic if I could have used an enum for the units
/// and I wrote it this way under duress because that's what the assignment asked for to teach about lifetimes.

pub fn start() {
    println!();
    println!("This program converts temperatures between Fahrenheit and Celcius.");
    println!("Acceptable inputs will be a number (with our without a decimal point)");
    println!("optionally followed by a letter indicating the unit");
    println!("'F' or 'f' for Fahrenheit, 'C' or 'c' for Celcius");
    println!("(if the unit is excluded, it will default to Fahrenheit)");
    println!("type 'exit' or 'quit' to leave the program.");
    println!();
    loop {
        let mut input = String::new();
        println!();
        println!("Input temperature: ");
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line!");

        // Don't want newlines or extraneous spaces gumming up string processing
        let trimmed_input = input.trim();

        match get_inputs(trimmed_input, "F") {
            Some((in_temp, in_unit, out_unit)) => {
                let out_temp = convert(in_temp, in_unit, out_unit);
                println!("{in_temp:.2}{in_unit} = {out_temp:.2}{out_unit}");
            }
            None => match trimmed_input.to_ascii_lowercase().as_str() {
                // Always have an exit strategy
                "exit" | "quit" => {
                    println!("Exiting...");
                    break;
                }
                _ => println!("Couldn't parse input..."),
            },
        }
    }
}

/// Attempts to convert the passed string into a temperature (number)
/// including checking for a trailing letter indicating unit
/// and using the given default unit if such a letter isn't present
///
/// For the regex inclined, this describes a valid input:
/// (-?\d+(\.\d+)?)\s*([FfCc]?)
/// **TODO**: update this to use the regex crate for cleaner processing of the textual input.
///
/// # Examples
///
/// ```
/// let (temp, in_unit, out_unit) = get_inputs("212.0F", "F").unwrap();
///
/// let parsed_input = get_inputs("32", "F");
/// ```
fn get_inputs<'a>(input_temp: &'a str, default_unit: &str) -> Option<(f64, &'a str, &'a str)> {
    match input_temp.chars().last() {
        Some(unit) => match unit {
            'F' | 'f' => {
                let (temp, _) = input_temp.split_at(input_temp.len() - 1);
                match temp.trim().parse::<f64>() {
                    Ok(num) => Some((num, "F", "C")),
                    Err(_) => None,
                }
            }
            'C' | 'c' => {
                let (temp, _) = input_temp.split_at(input_temp.len() - 1);
                match temp.trim().parse::<f64>() {
                    Ok(num) => Some((num, "C", "F")),
                    Err(_) => None,
                }
            }
            _ => match input_temp.trim().parse::<f64>() {
                Ok(num) => match default_unit {
                    "F" | "f" => Some((num, "F", "C")),
                    "C" | "c" => Some((num, "C", "F")),
                    _ => None,
                },
                Err(_) => None,
            },
        },
        None => None,
    }
}

/// Converts a temperature (represented as an f64) from F to C or C to F based on in/out units passed
/// If the in and out units are the same, it will just return the float
/// If you are a bad person and attempt to use unsupported units it will panic
///
/// Fun Fact: at -40 this will always return the same result with valid units because F & C cross over at that temp
///
/// # Examples
/// ```
/// let boiling_in_f = convert(100.0, "C", "F");
///
/// let freezing_in_c = convert(32.0, "F", "C");
/// ```
fn convert(in_temp: f64, in_unit: &str, out_unit: &str) -> f64 {
    match (in_unit, out_unit) {
        ("F", "C") => convert_to_c(in_temp),
        ("C", "F") => convert_to_f(in_temp),
        ("C", "C") | ("F", "F") => in_temp,
        (_, _) => panic!("Attempted unsupported conversion!"),
    }
}

/// Converts an f64 representing a temperature in Celcius to an f64 representing that same tempterature in Fahrenheit
/// # Examples
/// ```
/// let thirty_two = convert_to_f(0.0);
///
/// let two_twelve = convert_to_f(100.0);
fn convert_to_f(in_temp: f64) -> f64 {
    in_temp * 1.8 + 32.0
}

/// Converts an f64 representing a temperature in Fahrenheit to an f64 representing that same tempterature in Celcius
/// # Examples
/// ```
/// let zero = convert_to_c(32.0);
///
/// let one_hundred = convert_to_c(212.0);
/// ```
fn convert_to_c(in_temp: f64) -> f64 {
    (in_temp - 32.0) / 1.8
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_intputs() {
        assert_eq!(get_inputs("212".into(), "F"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("0.0".into(), "F"), Some((0.0, "F", "C")));
        assert_eq!(get_inputs("-40".into(), "F"), Some((-40.0, "F", "C")));
        assert_eq!(get_inputs("212f".into(), "F"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212F".into(), "F"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212f".into(), "C"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212F".into(), "C"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212 f".into(), "F"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212 F".into(), "F"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212 f".into(), "C"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212 F".into(), "C"), Some((212.0, "F", "C")));
        assert_eq!(get_inputs("212".into(), "C"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212c".into(), "F"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212C".into(), "F"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212c".into(), "C"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212C".into(), "C"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212 c".into(), "F"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212 C".into(), "F"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212 c".into(), "C"), Some((212.0, "C", "F")));
        assert_eq!(get_inputs("212 C".into(), "C"), Some((212.0, "C", "F")));
    }
    #[test]
    fn test_convert() {}
    #[test]
    fn test_convert_to_f() {
        assert_eq!(convert_to_f(0.0), 32.0);
        assert_eq!(convert_to_f(100.0), 212.0);
        assert_eq!(convert_to_f(-40.0), -40.0);
    }
    #[test]
    fn test_convert_to_c() {
        assert_eq!(convert_to_c(32.0), 0.0);
        assert_eq!(convert_to_c(212.0), 100.0);
        assert_eq!(convert_to_c(-40.0), -40.0);
    }
}

mod hw_one;

fn main() {
    loop {
        let mut input = String::new();
        println!();
        println!("Select a homework assignment to run: ");
        println!("0) exit/quit");
        println!("1) hw_one");
        std::io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line!");

        match input.trim() {
            "1" | "hw_one" => hw_one::start(),
            "0" | "exit" | "quit" => {
                println!("Exiting...");
                break;
            }
            "exit/quit" => {
                println!("Took that literally didn't you?");
                break;
            }
            _ => println!("Couldn't parse input..."),
        }
    }
}

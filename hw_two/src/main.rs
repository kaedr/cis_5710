use std::{
    cmp::{max, Ordering},
    collections::HashMap,
    env,
    error::Error,
    ffi::OsStr,
    fs::{self, File},
    io::{self, BufRead},
    os::unix::fs::MetadataExt,
    path::{Path, PathBuf},
    process,
    time::UNIX_EPOCH,
};
// In the spirit of no reinventing the wheel, I found a crate the converts unix permissions
// to the string form used by `ls -l`
use unix_mode;
// Similarly, I didn't want to have to rewrite time handling/formatting
use chrono::{self, DateTime, Datelike, Local, TimeZone, Utc};

// NOTE:
// I used the existing ls implementation from rust-coreutils as a reference
// when I got stuck at certain points in this assignment
// https://github.com/dkg/rust-coreutils/blob/master/src/uu/ls/src/ls.rs
//
// DISCLAIMER:
// The quality of the documentation drops off steeply on anything that was written
// or refactored after hour 20 of working on the assignement
// Which is kind of unfortunate, because I've refactored nearly everything since then


fn main() {
    let args: Vec<String> = env::args().collect();
    let (mut targets, flags) = match argparse(&args) {
        Ok(targets_and_flags) => targets_and_flags,
        Err(bad_flag) => {
            eprintln!("ls: invalid option -- '{bad_flag}'");
            eprintln!("Try 'ls --help' for more information.");
            process::exit(2);
        }
    };
    // display help and exit
    if flags.help {
        help();
        process::exit(0);
    }

    // Our default is always the current directory
    let here = String::from(".");
    if targets.is_empty() {
        targets.push(&here);
    }

    // Convert the Strings into Path, then further into HasMetaData to ease our future operations
    let mut targets: Vec<HasMetaData> = targets
        .iter()
        .map(|t| HasMetaData::from(Path::new(t.as_str())))
        .collect();

    // Sort our initial targets based on the incoming flags
    sort(&mut targets, &flags);

    // The directory flag supersedes a bunch of other options, and is handled more simply
    if !flags.directory {
        // When passed a list of targets, ls first handles any files then moves on to directories
        let mut files = Vec::new();
        let mut directories = Vec::new();
        for target in targets {
            if target.path.is_file() {
                files.push(target);
            } else {
                directories.push(target);
            }
        }
        if !files.is_empty() {
            // render the files
            render_direct(&files, &flags, true);
        }
        // then handle directories
        for directory in directories {
            list(directory.into(), &flags);
        }
    } else {
        render_direct(&targets, &flags, true);
    }
}

/// Enum for specify sort orders
#[derive(Debug, Default, PartialEq)]
enum Sort {
    #[default]
    Alphabetical,
    Time,
    Extension,
}

/// Stores a boolean representing each of the potential command line flags or
/// an enum for flags that represent mutually exlusive states as well as
/// keeping track of the current terminal column width for use with grid layout
/// it also includes lookup tables for uid/gid to username/groupname
///
/// Since these options are off by default and the default value for bool is false
/// and we set up default for Sort we are able to derive default here
#[derive(Debug, Default)]
struct Arguments {
    all: bool,
    by_line: bool,
    directory: bool,
    help: bool,
    long_format: bool,
    recursive: bool,
    reverse: bool,
    size: bool,
    sort: Sort,
    users: HashMap<u32, String>,
    groups: HashMap<u32, String>,
}

impl Arguments {
    /// Takes a string representing a command line flag
    /// and sets the appropriate boolean on the struct if the flag is valid
    /// otherwise returns the invalid flag as an Err
    fn set_option<'a>(&mut self, flag: &'a str) -> Result<(), &'a str> {
        match flag {
            "a" | "all" => {
                self.all = true;
                Ok(())
            }
            "1" => {
                self.by_line = true;
                Ok(())
            }
            "d" | "directory" => {
                self.directory = true;
                Ok(())
            }
            "X" => {
                self.sort = Sort::Extension;
                Ok(())
            }
            "help" => {
                self.help = true;
                Ok(())
            }
            "l" => {
                self.long_format = true;
                Ok(())
            }
            "R" | "recursive" => {
                self.recursive = true;
                Ok(())
            }
            "r" | "reverse" => {
                self.reverse = true;
                Ok(())
            }
            "s" | "size" => {
                self.size = true;
                Ok(())
            }
            "t" | "time" => {
                self.sort = Sort::Time;
                Ok(())
            }
            other => Err(other),
        }
    }

    /// Gets a username String based on user id
    fn user_lookup(&self, uid: u32) -> String {
        match self.users.get(&uid) {
            Some(name) => name.to_string(),
            None => String::from("???"),
        }
    }

    /// Gets a groupname String based on group id
    fn group_lookup(&self, gid: u32) -> String {
        match self.groups.get(&gid) {
            Some(name) => name.to_string(),
            None => String::from("???"),
        }
    }
}

/// Prints the help message
fn help() {
    println!(
        r#"
    Usage: ls [OPTION]... [FILE]...
    List information about the FILEs (the current directory by default).
    Sort entries alphabetically if none of -tX is specified.

    Options:
    -a, --all                  do not ignore entries starting with .
    -d, --directory            list directories themselves, not their contents
    -l                         use a long listing format
    -r, --reverse              reverse order while sorting
    -R, --recursive            list subdirectories recursively
    -s, --size                 print the allocated size of each file, in blocks
    -X                         sort alphabetically by entry extension
    -1                         list one file per line.
    -t                         sort by time, newest first
        --help     display this help and exit

    Exit status:
    0  if OK,
    1  if minor problems (e.g., cannot access subdirectory),
    2  if serious trouble (e.g., cannot access command-line argument).
    "#
    );
}

/// Takes the command line arguments given and either return a tuple containing
/// references to the files/directories specified and a struct representing the flags
/// or a &str representing an invalid flag that was encountered
fn argparse(args: &Vec<String>) -> Result<(Vec<&String>, Arguments), &str> {
    let mut flags: Arguments = Default::default();
    let mut targets: Vec<&String> = Vec::new();
    // args[0] will always be the name of our executeable, so it is desireable
    // to exclude that from our operations.
    for arg in args[1..].iter() {
        if arg.starts_with("--") {
            flags.set_option(arg.trim_start_matches("-"))?;
        } else if arg.starts_with("-") {
            // Found the trick for &str.split_inclusive on reddit after some searching.
            // The &str.chars() method causes problems because the Chars it yields can't
            // be converted back into a &str without running into lifetime issues
            // In practical terms, split_inclusive includes the character that's being split on
            // and we're passing it a closure that returns true for every character, resulting
            // in the character by character split as &str that we're looking for
            for option in arg.trim_start_matches("-").split_inclusive(|_| true) {
                flags.set_option(option)?;
            }
        } else {
            targets.push(arg);
        }
    }
    flags.users = build_id_lookup_table(&Path::new("/etc/passwd"));
    flags.groups = build_id_lookup_table(&Path::new("/etc/group"));
    Ok((targets, flags))
}

/// Takes a path to a file that is structured after the fashion of /etc/passwd & /etc/group
/// and returns a hashmap with the uid/gid -> username/groupname mappings
fn build_id_lookup_table(file: &Path) -> HashMap<u32, String> {
    let mut table = HashMap::new();
    if let Ok(file) = File::open(file) {
        let lines = io::BufReader::new(file).lines();
        for line in lines {
            if let Ok(entry) = line {
                let entry: Vec<&str> = entry.split(":").collect();
                if let (Some(name), Some(id)) = (entry.get(0), entry.get(2)) {
                    if let Ok(id) = id.parse::<u32>() {
                        table.insert(id, name.to_string());
                    }
                }
            }
        }
    }
    table
}

/// Output the contents of the specified PathBuf in keeping with the &Arguments given
/// Return an i32 representing the *nix process return status that has been generated
fn list(path: PathBuf, flags: &Arguments) -> i32 {
    let entries = match fs::read_dir(&path) {
        Ok(entries) => entries,
        Err(err_msg) => {
            eprintln!(
                "ls: cannot access '{}': {}",
                path.as_os_str().to_str().unwrap(),
                err_msg
            );
            return 1;
        }
    };
    // Vec of status codes for recursive calls
    let mut status_codes = Vec::new();
    // Actual entries that we can see/interact with
    let mut vetted_entries = Vec::new();
    // Handle -a/--all
    if flags.all {
        vetted_entries.push(HasMetaData {
            path: PathBuf::from("."),
        });
        vetted_entries.push(HasMetaData {
            path: PathBuf::from(".."),
        });
    }

    // Go through and process our entries, handling any errors
    for entry in entries {
        match entry {
            Ok(entry_actual) => {
                if flags.all || !entry_actual.file_name().to_string_lossy().starts_with(".") {
                    vetted_entries.push(HasMetaData::from(entry_actual));
                }
            }
            Err(err_msg) => {
                eprintln!(
                    "ls: cannot access '{}': {}",
                    path.as_os_str().to_str().unwrap(),
                    err_msg
                );
                status_codes.push(1);
            }
        };
    }

    sort(&mut vetted_entries, flags);

    // Print directory heading if recursive call
    if flags.recursive {
        println!("{}:", HasMetaData::from(path).relative_name_string());
    }
    render(&vetted_entries, flags);

    if flags.recursive {
        for entry in vetted_entries {
            if entry.path.is_dir() {
                println!();
                status_codes.push(list(entry.path, flags))
            }
        }
    }
    return *status_codes.iter().max().unwrap_or(&0);
}

/// Performs an in place sort on a mutable vector of HasMetaData based on the flags given
fn sort(items: &mut Vec<HasMetaData>, flags: &Arguments) {
    // Always sort alphabetically first so things are alphabetical within other sort orders
    items.sort_by(|a, b| a.name_string().cmp(&b.name_string()));
    match flags.sort {
        Sort::Alphabetical => (),
        Sort::Time => items.sort_by(|a, b| a.time_ord(b)),
        Sort::Extension => items.sort_by(|a, b| a.extension_ord(b)),
    }
    if flags.reverse {
        items.reverse();
    }
}

/// Renders the items in accordance with the flags
/// Also dispatches some special behavior if we've been told that everything
/// to be rendered is being given to us directly and not walked from a directory
fn render_direct(items: &Vec<HasMetaData>, flags: &Arguments, direct: bool) {
    // Gather data about the items to be displayed
    let paddings = Paddings::gather(items, flags, direct);
    if flags.long_format {
        if !flags.directory {
            println!("total {}", paddings.total_blocks);
        }
        for item in items {
            render_long(item, flags, &paddings, direct)
        }
    } else if flags.by_line {
        if flags.size && !flags.directory {
            println!("total {}", paddings.total_blocks);
        }
        for item in items {
            render_by_line(item, flags, &paddings, direct);
        }
    } else {
        if flags.size && !flags.directory {
            println!("total {}", paddings.total_blocks);
        }
        render_grid(items, flags, &paddings, direct)
    }
}

/// Shorthand for rendering when items are not being directly provided
fn render(items: &Vec<HasMetaData>, flags: &Arguments) {
    render_direct(items, flags, false)
}

/// Outputs the long form render of the information about this HasMetaData
fn render_long(item: &HasMetaData, flags: &Arguments, paddings: &Paddings, direct: bool) {
    if flags.size {
        print!("{:>width$} ", item.block_string(), width = paddings.blocks);
    }
    print!("{} ", item.permission_string());
    print!("{:>width$} ", item.link_string(), width = paddings.links);
    print!("{:<width$} ", item.user(flags), width = paddings.user);
    print!("{:<width$} ", item.group(flags), width = paddings.group);
    print!("{:>width$} ", item.size_string(), width = paddings.size);
    print!("{} ", item.modified_string());
    println!("{}", item.long_name_string(direct));
}

/// Returns the name of the given HasMetaData appropriately formatted
/// based on the flags, paddings, and whether or not it was called direct
fn arrange_name(
    item: &HasMetaData,
    flags: &Arguments,
    paddings: &Paddings,
    direct: bool,
) -> String {
    let name = if !direct {
        item.name_string()
    } else {
        item.clean_rel_name()
    };
    let (blocks_width, name_width) = (paddings.blocks, paddings.name);
    if flags.size {
        format!(
            "{:>blocks_width$} {:<name_width$}",
            item.block_string(),
            name,
        )
    } else {
        format!("{:<name_width$}", name)
    }
}

/// Outputs the simple by line render of the -1 flag
fn render_by_line(item: &HasMetaData, flags: &Arguments, paddings: &Paddings, direct: bool) {
    println!("{}", arrange_name(item, flags, paddings, direct).trim_end());
}

/// Outputs the default grid based render of ls
///
/// The width of the columns is the same across all columns which differs from the gnu/linux
/// implentation, but I couldn't think of a simple way to change it without having to iterate
/// through everything multiple times to get the spacing right
fn render_grid(items: &Vec<HasMetaData>, flags: &Arguments, paddings: &Paddings, direct: bool) {
    // I tried everything I could think of to get this outside or relying on an external crate
    // But everything method I tried resulted in 80. Turns out this is because of subshell
    // behavior, and since any system calls or environment variable checks rust makes run in
    // a subshell context, the default terminal width of 80 is all the program can see
    let term_cols = match termsize::get() {
        Some(size) => size.cols.into(),
        None => 80,
    };
    if term_cols > paddings.total_name {
        for item in items {
            print!("{}  ", arrange_name(item, flags, paddings, direct).trim());
        }
        if !items.is_empty() {
            println!();
        }
    } else {
        let col_width = paddings.name + if flags.size { paddings.blocks + 3 } else { 3 };
        let columns: usize = term_cols / col_width;
        let items_per_column= items.len() / columns + 1;
        let chunks: Vec<&[HasMetaData]> = items.chunks(items_per_column).collect();
        for index in 0..items_per_column {
            for column in 0..columns {
                if let Some(item) = chunks.get(column) {
                    if let Some(sub_item) = item.get(index) {
                        print!("{}  ", arrange_name(sub_item, flags, paddings, direct));
                    } else {
                        print!("{: <width$}", "", width = col_width);
                    }
                } else {
                    print!("{: <width$}", "", width = col_width);
                }
            }
            println!();
        }
    }
}

/// Store all the padding information for rendering our output
#[derive(Debug)]
struct Paddings {
    total_blocks: u64,
    blocks: usize,
    links: usize,
    user: usize,
    group: usize,
    size: usize,
    total_name: usize,
    name: usize,
}

impl Paddings {
    /// Walk through all the items and gather up their padding measurements
    /// based on the flags provided and whether or not a direct render is being
    /// requested
    fn gather(items: &Vec<HasMetaData>, flags: &Arguments, direct: bool) -> Self {
        let mut total_blocks: u64 = 0;
        let mut blocks: usize = 0;
        let mut links: usize = 0;
        let mut user: usize = 0;
        let mut group: usize = 0;
        let mut size: usize = 0;
        let mut total_name: usize = 0;
        let mut name: usize = 0;

        for item in items {
            let name_string = if !direct {
                item.name_string()
            } else {
                item.clean_rel_name()
            };
            total_blocks = total_blocks + item.blocks().unwrap_or(0);
            blocks = max(blocks, item.block_string().len());
            links = max(links, item.link_string().len());
            user = max(user, item.user(flags).len());
            group = max(group, item.group(flags).len());
            size = max(size, item.size_string().len());
            total_name = total_name
                + name_string.len()
                + 2
                + if flags.size {
                    item.size_string().len() + 1
                } else {
                    0
                };
            name = max(name, name_string.len());
        }
        Paddings {
            total_blocks,
            blocks,
            links,
            user,
            group,
            size,
            total_name,
            name,
        }
    }
}

/// HasMetaData wraps a PathBuf and allows us to implement needed methods
#[derive(Debug)]
struct HasMetaData {
    path: PathBuf,
}

impl From<fs::DirEntry> for HasMetaData {
    fn from(dir_entry: fs::DirEntry) -> Self {
        HasMetaData {
            path: dir_entry.path(),
        }
    }
}

impl From<&Path> for HasMetaData {
    fn from(path: &Path) -> Self {
        HasMetaData {
            path: path.to_path_buf(),
        }
    }
}

impl From<PathBuf> for HasMetaData {
    fn from(path: PathBuf) -> Self {
        HasMetaData { path: path }
    }
}

impl From<HasMetaData> for PathBuf {
    fn from(hmd: HasMetaData) -> Self {
        hmd.path
    }
}

impl HasMetaData {
    /// Returns the extension (if any) of this HasMetaData
    fn extension(&self) -> Option<&OsStr> {
        self.path.extension()
    }

    /// Returns the metadata of this HasMetaData
    fn metadata(&self) -> Result<fs::Metadata, std::io::Error> {
        if !self.path.is_symlink() {
            self.path.metadata()
        } else {
            self.path.symlink_metadata()
        }
    }

    /// Returns the string form of the number of blocks allocated to this file
    fn block_string(&self) -> String {
        match self.blocks() {
            Ok(blocks) => blocks.to_string(),
            Err(_) => String::from("? "),
        }
    }

    fn blocks(&self) -> Result<u64, std::io::Error> {
        // NOTE!:
        // This line includes division by 2 because rust is assuming 512 byte blocks
        // and ls on two of my linux machines uses 1024 byte blocks
        // which also means that the gnu/linux ls implementation is wrong, because the actual
        // block size on both systems is 4096
        // but I decided it was better to be wrong in the same way as the gnu implentation
        // I'd be really interested to know what the ls behavior is on your Mac
        Ok(self.metadata()?.blocks() / 2)
    }

    /// Returns a string representing the unix permissions on the file/directory
    /// If there are issues reading the metadata, and appropriately sized string
    /// of question marks is returned
    fn permission_string(&self) -> String {
        match self.metadata() {
            Ok(meta) => unix_mode::to_string(meta.mode()),
            Err(_) => String::from("??????????"),
        }
    }

    /// Returns a string representing the number of links to this file
    fn link_string(&self) -> String {
        match self.metadata() {
            Ok(meta) => meta.nlink().to_string(),
            Err(_) => String::from("?"),
        }
    }

    /// Returns a username based on a uid and the lookup table in flags
    fn user(&self, flags: &Arguments) -> String {
        match self.metadata() {
            Ok(meta) => flags.user_lookup(meta.uid()),
            Err(_) => String::from("???"),
        }
    }

    /// Returns a groupname based on a gid and the lookup table in flags
    fn group(&self, flags: &Arguments) -> String {
        match self.metadata() {
            Ok(meta) => flags.group_lookup(meta.gid()),
            Err(_) => String::from("???"),
        }
    }

    /// Returns a string representing the size in bytes of the file/directory
    fn size_string(&self) -> String {
        match self.metadata() {
            Ok(meta) => meta.len().to_string(),
            Err(_) => String::from("?"),
        }
    }

    /// Returns a String representing the modified time
    /// This String will be a bunch of question marks if an error occurred
    fn modified_string(&self) -> String {
        match self.modified_helper() {
            Ok(mod_str) => mod_str,
            Err(_) => String::from("??? ?? ??:??"),
        }
    }

    /// Returns a result of the modified time represented as a string or one
    /// of the many errors that can arise attempting that conversion
    /// This was necessary to simplify error handling
    fn modified_helper(&self) -> Result<String, Box<dyn Error>> {
        let mod_time = self
            .metadata()?
            .modified()?
            .duration_since(UNIX_EPOCH)?
            .as_secs()
            .try_into()?;
        let local_mod_time: DateTime<Local> = Utc.timestamp_opt(mod_time, 0).unwrap().into();
        if local_mod_time.year() == Local::now().year() {
            Ok(format!("{}", local_mod_time.format("%b %d %H:%M")))
        } else {
            Ok(format!("{}", local_mod_time.format("%b %d  %Y")))
        }
    }

    /// Returns the basename of this HasMetaData
    fn name_string(&self) -> String {
        // Apparently certain special paths (those handled here) don't have a 'file name'
        // the way that most other paths do, which leads to this rather annoying manual
        // if/else handling of those cases
        if self.path == Path::new(".") {
            String::from(".")
        } else if self.path == Path::new("..") {
            String::from("..")
        } else if self.path == Path::new("/") {
            String::from("/")
        } else {
            match self.path.file_name() {
                Some(file_name) => file_name.to_string_lossy().to_string(),
                None => String::from("???"),
            }
        }
    }

    /// Returns the relative path of this HasMetaData
    fn relative_name_string(&self) -> String {
        self.path.as_os_str().to_string_lossy().to_string()
    }

    /// Returns the relative path of this HasMetaData with any leading `./` removed
    fn clean_rel_name(&self) -> String {
        self.relative_name_string()
            .trim_start_matches("./")
            .to_string()
    }

    /// Returns the path of what a symlink is pointing to
    fn link_name_string(&self) -> String {
        match self.path.read_link() {
            Ok(link_path) => link_path.as_os_str().to_str().unwrap_or("???").into(),
            Err(_) => String::from("???"),
        }
    }

    /// Returns long form name for this file/dir/symlink, including what a symlink is pointing to
    fn long_name_string(&self, direct: bool) -> String {
        let mut basename = if !direct {
            self.name_string()
        } else {
            self.clean_rel_name()
        };
        if self.path.is_symlink() {
            basename.push_str(" -> ");
            basename.push_str(&self.link_name_string());
        }
        basename
    }

    /// Returns an ordering of the timestamps of this and another HasMetaData
    fn time_ord(&self, other: &Self) -> Ordering {
        match (self.metadata(), other.metadata()) {
            (Ok(a_meta), Ok(b_meta)) => match (a_meta.modified(), b_meta.modified()) {
                (Ok(a_mod), Ok(b_mod)) => {
                    match a_mod.partial_cmp(&b_mod).unwrap_or(Ordering::Equal) {
                        // Rust's default sort order naturally puts smaller/older times first and larger/newer times last
                        // So we need to invert the ordering for our time sort to work correctly
                        Ordering::Less => Ordering::Greater,
                        Ordering::Equal => Ordering::Equal,
                        Ordering::Greater => Ordering::Less,
                    }
                }
                (Ok(_), Err(_)) => Ordering::Greater,
                (Err(_), Ok(_)) => Ordering::Less,
                (Err(_), Err(_)) => Ordering::Equal,
            },
            (Ok(_), Err(_)) => Ordering::Greater,
            (Err(_), Ok(_)) => Ordering::Less,
            (Err(_), Err(_)) => Ordering::Equal,
        }
    }

    /// Returns an ordering of the extensions of this and another HasMetaData
    fn extension_ord(&self, other: &Self) -> Ordering {
        self.extension().cmp(&other.extension())
    }
}


/// FULL DISCLOSURE:
/// I know these tests have pitiful code coverage
/// This is due to a combination of factors, my main excuse being that I've
/// never had to mock out filesystem io for rust tests before and didn't feel like
/// adding that particular learning experience onto the time required for this assignment
///
/// That said, in hindsight, I kinda wish I had, there are a bunch of small issues I ran into
/// in my final use case testing that I'd have caught much earlier if I'd taken the time to
/// figure out how to build out the tests. Also my code would probably look a lot nicer
/// and be more modular. Bridget would be ashamed of me.
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_argument_set_option_all() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.all);
        assert_eq!(Ok(()), test_args.set_option("a"));
        assert!(test_args.all);
        test_args.all = false;
        assert_eq!(Ok(()), test_args.set_option("all"));
        assert!(test_args.all);
    }

    #[test]
    fn test_argument_set_option_by_line() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.by_line);
        assert_eq!(Ok(()), test_args.set_option("1"));
        assert!(test_args.by_line);
    }

    #[test]
    fn test_argument_set_option_directory() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.directory);
        assert_eq!(Ok(()), test_args.set_option("d"));
        assert!(test_args.directory);
        test_args.directory = false;
        assert_eq!(Ok(()), test_args.set_option("directory"));
        assert!(test_args.directory);
    }

    #[test]
    fn test_argument_set_option_help() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.help);
        assert_eq!(Ok(()), test_args.set_option("help"));
        assert!(test_args.help);
    }

    #[test]
    fn test_argument_set_option_long_format() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.long_format);
        assert_eq!(Ok(()), test_args.set_option("l"));
        assert!(test_args.long_format);
    }

    #[test]
    fn test_argument_set_option_recursive() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.recursive);
        assert_eq!(Ok(()), test_args.set_option("R"));
        assert!(test_args.recursive);
        test_args.recursive = false;
        assert_eq!(Ok(()), test_args.set_option("recursive"));
        assert!(test_args.recursive);
    }

    #[test]
    fn test_argument_set_option_reverse() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.reverse);
        assert_eq!(Ok(()), test_args.set_option("r"));
        assert!(test_args.reverse);
        test_args.reverse = false;
        assert_eq!(Ok(()), test_args.set_option("reverse"));
        assert!(test_args.reverse);
    }

    #[test]
    fn test_argument_set_option_size() {
        let mut test_args: Arguments = Default::default();
        assert!(!test_args.size);
        assert_eq!(Ok(()), test_args.set_option("s"));
        assert!(test_args.size);
        test_args.size = false;
        assert_eq!(Ok(()), test_args.set_option("size"));
        assert!(test_args.size);
    }

    #[test]
    fn test_argument_set_option_time_sort() {
        let mut test_args: Arguments = Default::default();
        assert_eq!(Sort::Alphabetical, test_args.sort);
        assert_eq!(Ok(()), test_args.set_option("t"));
        assert_eq!(Sort::Time, test_args.sort);
        test_args.sort = Sort::Alphabetical;
        assert_eq!(Ok(()), test_args.set_option("time"));
        assert_eq!(Sort::Time, test_args.sort);
    }

    #[test]
    fn test_argument_set_option_extension_sort() {
        let mut test_args: Arguments = Default::default();
        assert_eq!(Sort::Alphabetical, test_args.sort);
        assert_eq!(Ok(()), test_args.set_option("X"));
        assert_eq!(Sort::Extension, test_args.sort);
    }

    #[test]
    fn test_argument_set_option_sort_overrides() {
        let mut test_args: Arguments = Default::default();
        assert_eq!(Sort::Alphabetical, test_args.sort);
        assert_eq!(Ok(()), test_args.set_option("X"));
        assert_eq!(Sort::Extension, test_args.sort);
        assert_eq!(Ok(()), test_args.set_option("t"));
        assert_eq!(Sort::Time, test_args.sort);
        assert_eq!(Ok(()), test_args.set_option("X"));
        assert_eq!(Sort::Extension, test_args.sort);
    }

    #[test]
    fn test_argument_set_option_invalid_numbers() {
        let mut test_args: Arguments = Default::default();
        assert_eq!(Err("12"), test_args.set_option("12"));
        assert_eq!(Err("2"), test_args.set_option("2"));
        assert_eq!(Err("8675309"), test_args.set_option("8675309"));
    }

    #[test]
    fn test_argument_set_option_invalid_letters() {
        let mut test_args: Arguments = Default::default();
        assert_eq!(Err("z"), test_args.set_option("z"));
        assert_eq!(Err("P"), test_args.set_option("P"));
        assert_eq!(Err("j"), test_args.set_option("j"));
    }

    #[test]
    fn test_argument_set_option_invalid_words() {
        let mut test_args: Arguments = Default::default();
        assert_eq!(Err("most"), test_args.set_option("most"));
        assert_eq!(Err("bigly"), test_args.set_option("bigly"));
        assert_eq!(Err("alf"), test_args.set_option("alf"));
    }

    #[test]
    fn test_argparse_simple() {
        let test_inputs: Vec<String> = ["/bin/ls", "-t", "-s", "src"].map(String::from).to_vec();
        let (test_args, flags) = argparse(&test_inputs).unwrap();
        assert_eq!(vec!["src"], test_args);
        assert_eq!(Sort::Time, flags.sort);
        assert!(flags.size);
    }

    #[test]
    fn test_argparse_complex() {
        let test_inputs: Vec<String> = [
            "/bin/ls",
            "-sX",
            "--recursive",
            "src",
            "target",
            "--directory",
        ]
        .map(String::from)
        .to_vec();
        let (test_args, flags) = argparse(&test_inputs).unwrap();
        assert_eq!(vec!["src", "target"], test_args);
        assert_eq!(Sort::Extension, flags.sort);
        assert!(flags.size);
        assert!(flags.recursive);
        assert!(flags.directory);
    }

    #[test]
    fn test_argparse_invalid() {
        let test_inputs: Vec<String> = ["/bin/ls", "-e", "src", "target", "--directory"]
            .map(String::from)
            .to_vec();
        match argparse(&test_inputs) {
            Ok(_) => assert!(false, "Expected Err, got Ok!"),
            Err(message) => assert_eq!("e", message),
        }
    }
}

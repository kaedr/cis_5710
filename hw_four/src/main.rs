use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use crate::risk_board::GameBoard;

fn main() {
    let mut the_game = GameBoard::from(read_lines("./risk_board.txt").unwrap());
    println!("Welcome to Risk!");
    let mut num_players = 0;
    while !(2..=6).contains(&num_players) {
        println!("Please enter the number of players (2-6):");
        let user_input = get_input();
        num_players = match user_input.trim().parse() {
            Ok(num) => num,
            Err(err) => {
                println!("{}", err);
                0
            }
        }
    }

    for player_num in 1..=num_players {
        println!("Please enter a name for player {}:", player_num);
        let name = get_input();

        the_game.add_player(name);
    }

    let num_armies = match num_players {
        2 => 40,
        3 => 35,
        4 => 30,
        5 => 25,
        6 => 20,
        _ => panic!("Number of Players {} outside valid range!", num_players),
    };

    for _ in 0..num_armies {
        for (player_id, player) in the_game.players().iter().enumerate() {
            println!("Army placement for {}", player);
            loop {
                println!("Please enter a territory for placement:");
                let territory = get_input();
                match the_game.place_army(territory, player_id) {
                    Ok(_) => break,
                    Err(err) => println!("{}", err),
                }
            }
        }
    }
    the_game.start_game()
}

fn get_input() -> String {
    let mut user_input = String::new();
    io::stdin()
        .read_line(&mut user_input)
        .expect("Failed to read user input");
    user_input.trim().into()
}

// This function shamelessly ripped from rust by example https://doc.rust-lang.org/rust-by-example/std_misc/file/read_lines.html
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

mod risk_board {
    use std::{
        cell::RefCell,
        collections::HashMap,
        fmt::Display,
        fs::File,
        io::{BufReader, Lines},
    };

    use hw_four::Graph;

    /// Name of a given Territory
    type Territory = String;
    /// Numeric ID of a given territory
    type TerritoryId = usize;
    /// Name of a given Continent
    type ContinentName = String;
    /// Numeric ID of a given Continent
    type ContinentId = usize;
    /// Numeric ID of a given Player
    type PlayerId = usize;

    /// Colors for representing the armies of up to 6 players
    #[derive(Debug)]
    pub enum Color {
        Red,
        Orange,
        Yellow,
        Green,
        Blue,
        Purple,
    }

    impl Color {
        pub fn from_usize(value: usize) -> Self {
            match value {
                0 => Color::Red,
                1 => Color::Orange,
                2 => Color::Yellow,
                3 => Color::Green,
                4 => Color::Blue,
                5 => Color::Purple,
                _ => panic!("Player number {} outside valid range!", value),
            }
        }
    }

    impl Display for Color {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{:?}", self)
        }
    }

    /// A Player in the game, having a name and a color for their tokens
    pub struct Player {
        name: String,
        color: Color,
    }

    impl Display for Player {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{} ({})", self.name, self.color)
        }
    }

    /// A Continent on the game board, it has a name, a value, and a collection of territories that belong to it
    pub struct Continent {
        name: String,
        value: usize,
        territories: Vec<TerritoryId>,
    }

    /// An Army on the board, it belongs to a player and has a size
    pub struct Army {
        player: PlayerId,
        size: usize,
    }

    /// A Game Board for Risk, it tracks all the Players, continents, territories, connections between territories,
    /// army placements, and whether or not the game is currently active (initial troop placement is complete)
    pub struct GameBoard {
        players: Vec<Player>,
        continents: Vec<Continent>,
        territories: Vec<Territory>,
        connections: Graph<TerritoryId, ContinentId>,
        army_placements: RefCell<HashMap<TerritoryId, Army>>,
        active: bool,
    }

    impl GameBoard {
        pub fn new() -> Self {
            GameBoard {
                players: Vec::new(),
                territories: Vec::new(),
                connections: Graph::new(),
                continents: Vec::new(),
                army_placements: RefCell::new(HashMap::new()),
                active: false,
            }
        }

        pub fn start_game(&mut self) {
            self.active = true;
        }

        pub fn add_player(&mut self, name: String) -> PlayerId {
            self.players.push(Player {
                name,
                color: Color::from_usize(self.players.len()),
            });
            self.players.len() - 1
        }

        pub fn players(&self) -> &Vec<Player> {
            &self.players
        }

        fn add_continent(&mut self, name: ContinentName, value: usize) -> ContinentId {
            self.continents.push(Continent {
                name,
                value,
                territories: Vec::new(),
            });
            self.continents.len() - 1
        }

        fn add_territory(
            &mut self,
            territory: Territory,
            continent: ContinentId,
        ) -> Result<TerritoryId, String> {
            self.territories.push(territory);
            let territory_id = self.territories.len() - 1;
            self.connections.add_node(territory_id, continent)?;
            Ok(territory_id)
        }

        fn add_connection(&mut self, territory_a: TerritoryId, territory_b: TerritoryId) -> bool {
            self.connections.join_nodes(&territory_a, &territory_b)
        }

        pub fn place_army(&self, territory: Territory, player_id: PlayerId) -> Result<(), String> {
            if let Some(territory) = self.get_territory_id(&territory) {
                // Enable setup rule checks
                if !self.active
                    && self.army_placements.borrow().len() < self.territories.len()
                    && self.army_placements.borrow().contains_key(&territory)
                {
                    return Err(String::from(
                        "All territories must be claimed before troops can be stacked",
                    ));
                }
                self.add_army(territory, player_id)
            } else {
                Err(format!("No territory '{}' found on game board.", territory))
            }
        }

        fn add_army(&self, territory: TerritoryId, player_id: PlayerId) -> Result<(), String> {
            let mut mut_placements = self.army_placements.borrow_mut();
            if let Some(army) = mut_placements.get_mut(&territory) {
                if army.player != player_id {
                    return Err(String::from(
                        "Cannot add army on territory occupied by enemy forces!",
                    ));
                }
                army.size += 1;
            } else {
                mut_placements.insert(
                    territory,
                    Army {
                        player: player_id,
                        size: 1,
                    },
                );
            }
            Ok(())
        }

        fn get_territory_id(&self, name: &str) -> Option<TerritoryId> {
            self.territories.iter().position(|t| t == name)
        }
    }

    impl From<Lines<BufReader<File>>> for GameBoard {
        fn from(file: Lines<BufReader<File>>) -> Self {
            let mut new_board = GameBoard::new();
            let mut current_continent = 0;
            let mut current_territory = 0;
            for line in file.flatten() {
                if line.starts_with("    : ") {
                    let adjoining_territory = line.trim_start_matches("    : ");
                    let adj_terr_id = new_board
                        .get_territory_id(adjoining_territory)
                        .expect("Referenced undefined territory!");
                    if !new_board.add_connection(current_territory, adj_terr_id) {
                        panic!("Attempted to parse invalid connection!");
                    }
                } else if line.starts_with("  ") {
                    let territory = line.trim_start().into();
                    current_territory = new_board
                        .add_territory(territory, current_continent)
                        .unwrap();
                } else {
                    let (cont_name, cont_val) =
                        line.rsplit_once(' ').expect("Improper continent format!");
                    let value = cont_val.parse().expect("Improper continent format!");
                    let name = String::from(cont_name);
                    current_continent = new_board.add_continent(name, value);
                }
            }
            new_board
        }
    }
}

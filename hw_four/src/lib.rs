use std::{cell::RefCell, rc::Rc};

pub fn add(left: usize, right: usize) -> usize {
    left + right
}

pub struct Graph<L, V> {
    node_list: Vec<Rc<Node<L, V>>>,
}

impl<L, V> Graph<L, V>
where
    L: PartialEq,
{
    /// Make a new, empty `Graph`
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// // Items can be added with add_node
    /// graph.add_node(42, "Is the answer.");
    /// ```
    pub fn new() -> Self {
        Graph {
            node_list: Vec::new(),
        }
    }

    /// Adds the given label and value as a new node in the Graph
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// assert!(!graph.contains(&"B"));
    /// graph.add_node("B", 52);
    /// assert!(graph.contains(&"B"));
    /// ```
    pub fn add_node(&mut self, label: L, value: V) -> Result<(), String> {
        let new_node = Rc::new(Node::new(label, value));
        if self.node_list.contains(&new_node) {
            return Err(String::from("Node with label already exists in graph!"));
        }
        self.node_list.push(new_node);
        Ok(())
    }

    /// Returns the number of nodes in the graph
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// assert_eq!(0, graph.num_nodes());
    /// graph.add_node("F", 16);
    /// assert_eq!(1, graph.num_nodes());
    /// graph.add_node("A", 10);
    /// assert_eq!(2, graph.num_nodes());
    /// ```
    pub fn num_nodes(&self) -> usize {
        self.node_list.len()
    }

    /// Takes two labels and makes a one-way connection between them,
    /// returns a boolean indicating success or failure
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// graph.add_node("B", 2);
    /// graph.add_node("EA", 18);
    /// assert!(graph.connect_nodes(&"B", &"EA"));
    /// // Fails on already existing edges
    /// assert!(!graph.connect_nodes(&"B", &"EA"));
    /// // Fails on non-existent labels
    /// assert!(!graph.connect_nodes(&"B", &"Q"));
    /// ```
    pub fn connect_nodes(&self, source_label: &L, dest_label: &L) -> bool {
        if let Some(source) = self.get(source_label) {
            if let Some(dest) = self.get(dest_label) {
                if !source.neighbors.borrow().contains(&dest) {
                    source.neighbors.borrow_mut().push(dest);
                    return true;
                }
            }
        }
        false
    }

    /// Takes two labels and makes a two-way connection between them,
    /// returns a boolean indicating success or failure
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// graph.add_node("KC", 135);
    /// graph.add_node("AC", 130);
    /// assert!(graph.join_nodes(&"KC", &"AC"));
    /// // Fails on already existing edges
    /// assert!(!graph.join_nodes(&"KC", &"AC"));
    /// // Fails on non-existent labels
    /// assert!(!graph.join_nodes(&"KC", &"Q"));
    /// ```
    pub fn join_nodes(&self, source_label: &L, dest_label: &L) -> bool {
        let forward = self.connect_nodes(source_label, dest_label);
        let reverse = self.connect_nodes(dest_label, source_label);
        // If we succeed at making either link, that means both labels exist, just that one of them
        // was already referencing the other, so we succeed overall
        forward || reverse
    }

    /// Takes a label describing a starting point and a function that returns a boolean
    /// if a neighbor is of interest and returns a boolean indicating if any neighbor is of interest
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// graph.add_node("B", 21);
    /// graph.add_node("F", 35);
    /// assert!(graph.connect_nodes(&"B", &"F"));
    /// assert!(graph.any_neighbor(&"B", |x, y| x < y));
    /// ```
    pub fn any_neighbor<F>(&self, start_label: &L, interest_fn: F) -> bool
    where
        F: Fn(&V, &V) -> bool,
    {
        if let Some(start_node) = self.get(start_label) {
            start_node.any_neighbor(interest_fn)
        } else {
            false
        }
    }

    /// Takes a label describing a starting point and a function that takes a reference to a value
    /// and returns an orderable Key derived from that value
    /// This optionally returns the label of the node with the maximum node based on this ordering.
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// graph.add_node("F", 22);
    /// graph.add_node("C", 130);
    /// graph.add_node("KC", 46);
    /// assert!(graph.connect_nodes(&"F", &"C"));
    /// assert!(graph.connect_nodes(&"F", &"KC"));
    /// assert_eq!(&"C", graph.max_neighbor_by_key(&"F", |x| *x).unwrap());
    /// ```
    pub fn max_neighbor_by_key<F, K>(&self, start_label: &L, key_fn: F) -> Option<&L>
    where
        F: Fn(&V) -> K,
        K: Ord,
    {
        // I feel like there has to be a better way of handling this but I ran in circles with
        // ownership and lifetimes for long enough that I decided to go with something that worked
        // rather than burn a bunch more time looking for an optimal solution
        if let Some(start_node) = self.get_ref(start_label) {
            if let Some(max_node) = start_node.max_neighbor_by_key(key_fn) {
                if let Some(node_ref) = self.get_ref(&max_node.label) {
                    return Some(&node_ref.label);
                }
            }
        }
        None
    }

    /// Takes a label describing a node and Function that takes a value and
    /// will provide an updated value when called
    ///
    /// # Examples
    ///
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// graph.add_node("MQ", 9);
    /// graph.add_node("RQ", 4);
    /// graph.connect_nodes(&"MQ", &"RQ");
    /// graph.update_node(&"RQ", |_| 170);
    /// assert!(graph.any_neighbor(&"MQ", |_, y| &170 == y));
    /// ```
    pub fn update_node<F>(&self, label: &L, update_fn: F)
    where
        F: FnOnce(&mut V) -> V,
    {
        if let Some(node_rc) = self.get(label) {
            node_rc.value.replace_with(update_fn);
        }
    }

    /// Returns a boolean indicating if the given label exists in the graph
    ///
    /// # Examples
    /// ```
    /// use hw_four::Graph;
    ///
    /// let mut graph = Graph::new();
    /// assert!(!graph.contains(&"B"));
    /// graph.add_node("B", 1);
    /// assert!(graph.contains(&"B"));
    /// ```
    pub fn contains(&self, label: &L) -> bool {
        self.get(label).is_some()
    }

    /// Returns a clone of the Rc pointing to a node if the label exists in the graph
    fn get(&self, label: &L) -> Option<Rc<Node<L, V>>> {
        for node_rc in &self.node_list {
            if &node_rc.label == label {
                return Some(node_rc.clone());
            }
        }
        None
    }

    fn get_ref(&self, label: &L) -> Option<&Rc<Node<L, V>>> {
        self.node_list
            .iter()
            .find(|&node_rc| &node_rc.label == label)
    }
}

impl<L, V> Default for Graph<L, V>
where
    L: PartialEq,
{
    fn default() -> Self {
        Self::new()
    }
}

impl<L, V> From<Vec<(L, V)>> for Graph<L, V>
where
    L: PartialEq,
{
    fn from(values: Vec<(L, V)>) -> Self {
        let mut new_graph = Graph::new();
        for (label, value) in values {
            match new_graph.add_node(label, value) {
                Ok(_) => (),
                // This really should be a TryFrom impl with some kind of GraphConstraintError
                // But as this From is a convencience for me, not part of the assignment requirements
                // I a currently choosing to not invest the time
                Err(_) => todo!(),
            }
        }
        new_graph
    }
}

struct Node<L, V> {
    label: L,
    value: RefCell<V>,
    neighbors: RefCell<Vec<Rc<Node<L, V>>>>,
}

impl<L, V> Node<L, V> {
    fn new(label: L, value: V) -> Self {
        Node {
            label,
            value: RefCell::new(value),
            neighbors: RefCell::new(Vec::new()),
        }
    }

    fn any_neighbor<F>(&self, interest_fn: F) -> bool
    where
        F: Fn(&V, &V) -> bool,
    {
        for neighbor in &*self.neighbors.borrow() {
            if interest_fn(&self.value.borrow(), &neighbor.value.borrow()) {
                return true;
            }
        }
        false
    }

    fn max_neighbor_by_key<F, K>(&self, key_fn: F) -> Option<Rc<Node<L, V>>>
    where
        F: Fn(&V) -> K,
        K: Ord,
    {
        Some(
            self.neighbors
                .borrow()
                .iter()
                .max_by_key(|node| key_fn(&*node.value.borrow()))?
                .clone(),
        )
    }
}

impl<L, V> PartialEq for Node<L, V>
where
    L: PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.label == other.label
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_add_node_and_num_nodes() {
        let mut test_graph = Graph::new();
        match test_graph.add_node("A", 1) {
            Ok(_) => assert_eq!(1, test_graph.num_nodes()),
            Err(err) => assert!(false, "{}", err),
        }
        match test_graph.add_node("B", 2) {
            Ok(_) => assert_eq!(2, test_graph.num_nodes()),
            Err(err) => assert!(false, "{}", err),
        }
        match test_graph.add_node("B", 2) {
            Ok(_) => assert!(false, "Shouldn't be able to reinsert label!"),
            Err(err) => assert_eq!(
                String::from("Node with label already exists in graph!"),
                err
            ),
        }
    }

    #[test]
    fn test_from_vec_l_v_and_get() {
        let raw_nodes = vec![("A", 1), ("B", 2), ("C", 3), ("D", 4)];
        let test_graph = Graph::from(raw_nodes);
        assert_eq!(4, test_graph.num_nodes());
        match test_graph.get(&"C") {
            Some(node) => assert_eq!(3, *node.value.borrow()),
            None => assert!(false, "Graph does not contain label."),
        }
    }

    #[test]
    fn test_connect_nodes() {
        let raw_nodes = vec![("A", 1), ("B", 2), ("C", 3), ("D", 4)];
        let test_graph = Graph::from(raw_nodes);

        assert!(test_graph.connect_nodes(&"A", &"B"));
        let b_ref = &test_graph.get(&"B").unwrap();
        assert!(test_graph
            .get(&"A")
            .unwrap()
            .neighbors
            .borrow()
            .contains(b_ref));

        assert!(test_graph.connect_nodes(&"A", &"C"));
        let c_ref = &test_graph.get(&"C").unwrap();
        assert!(test_graph
            .get(&"A")
            .unwrap()
            .neighbors
            .borrow()
            .contains(c_ref));

        assert!(!test_graph.connect_nodes(&"D", &"Q"));
    }

    #[test]
    fn test_join_nodes() {
        let raw_nodes = vec![("A", 1), ("B", 2), ("C", 3), ("D", 4)];
        let test_graph = Graph::from(raw_nodes);
        assert!(test_graph.join_nodes(&"A", &"B"));
        let b_ref = &test_graph.get(&"B").unwrap();
        let a_ref = &test_graph.get(&"A").unwrap();
        assert!(a_ref.neighbors.borrow().contains(b_ref));
        assert!(b_ref.neighbors.borrow().contains(a_ref));
    }

    #[test]
    fn test_any_neighbor() {
        let raw_nodes = vec![("A", 1), ("B", 1), ("C", 3), ("D", 4)];
        let test_graph = Graph::from(raw_nodes);
        test_graph.connect_nodes(&"A", &"B");
        test_graph.connect_nodes(&"A", &"C");
        test_graph.connect_nodes(&"A", &"D");

        assert!(test_graph.any_neighbor(&"A", |x, y| x == y));
        assert!(!test_graph.any_neighbor(&"A", |x, y| x > y));
        assert!(test_graph.any_neighbor(&"A", |x, y| x < y));

        assert!(test_graph.any_neighbor(&"A", |x, y| x * 3 == *y));

        assert!(!test_graph.any_neighbor(&"A", |_, _| false));
    }

    #[test]
    fn test_max_neighbor_by_key() {
        let raw_nodes = vec![("A", 1), ("B", 2), ("C", 3), ("D", 4)];
        let test_graph = Graph::from(raw_nodes);
        test_graph.connect_nodes(&"A", &"B");
        test_graph.connect_nodes(&"A", &"C");
        test_graph.connect_nodes(&"A", &"D");

        assert_eq!(&"D", test_graph.max_neighbor_by_key(&"A", |x| *x).unwrap());
    }

    #[test]
    fn test_update_node() {
        let raw_nodes = vec![("A", 1), ("B", 2), ("C", 3), ("D", 4)];
        let test_graph = Graph::from(raw_nodes);
        test_graph.connect_nodes(&"A", &"B");
        test_graph.connect_nodes(&"A", &"C");
        test_graph.connect_nodes(&"A", &"D");

        assert_eq!(&"D", test_graph.max_neighbor_by_key(&"A", |x| *x).unwrap());
        test_graph.update_node(&"B", |x| *x * 22);
        assert_eq!(&"B", test_graph.max_neighbor_by_key(&"A", |x| *x).unwrap());
    }
}
